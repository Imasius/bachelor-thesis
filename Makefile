# Makefile to build the thesis PDF

.PHONY: aarch64-thesis.pdf all clean

all: aarch64-thesis.pdf

aarch64-thesis.pdf:	aarch64-thesis.tex
	latexmk -pdf -pdflatex="pdflatex -interactive=nonstopmode" -use-make aarch64-thesis.tex

clean:
	latexmk -CA
