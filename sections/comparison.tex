
\section{Comparing Alpha and AArch64}
\label{sec:comparison}

This chapter will highlight some of the differences between Alpha and AArch64. Although there are a lot of
subtle and not so subtle differences, this chapter will focus on the major ones in the 
respective instruction sets.


\subsection{Instruction set encoding and immediate size}

\begin{figure*}[ht]
\begin{bytefield}[rightcurly=., rightcurlyspace=0pt]{32}
    \bitheader[endianness=big]{0-31} \\

    \begin{rightwordgroup}{Integer operate}
    \bitbox{6}{Opcode} & \bitbox{5}{Ra} & \bitbox{5}{Rb} & \bitbox{3}{-} & \bitbox{1}{0} &
    \bitbox{7}{Function} & \bitbox{5}{Rc} 
    \end{rightwordgroup} \\

    \begin{rightwordgroup}{Integer operate, lit.}
    \bitbox{6}{Opcode} & \bitbox{5}{Ra} & \bitbox{8}{Literal} & \bitbox{1}{1} &
    \bitbox{7}{Function} & \bitbox{5}{Rc} 
    \end{rightwordgroup} \\

    \begin{rightwordgroup}{Floating-p. operate}
    \bitbox{6}{Opcode} & \bitbox{5}{Ra} & \bitbox{5}{Rb} &
    \bitbox{11}{Function} & \bitbox{5}{Rc} 
    \end{rightwordgroup} \\

    \begin{rightwordgroup}{Memory format}
    \bitbox{6}{Opcode} & \bitbox{5}{Ra} & \bitbox{5}{Rb} & \bitbox{16}{Displacement}
    \end{rightwordgroup} \\

    \begin{rightwordgroup}{Branch format}
    \bitbox{6}{Opcode} & \bitbox{5}{Ra} & \bitbox{21}{Displacement} 
    \end{rightwordgroup} \\

    \begin{rightwordgroup}{CALL\_PAL format}
    \bitbox{6}{Opcode} & \bitbox{26}{Displacement} 
    \end{rightwordgroup} \\
\end{bytefield}
\caption{Alpha instruction encoding}
\label{bf:alpha}
\end{figure*}

As seen in figure \ref{bf:alpha}, the instruction set encoding 
of Alpha can be split into 6 functional groups,
where each group shares an encoding pattern. AArch64 has these functional groups as well, but in contrast to
Alpha, AArch64 has lots of subgroups and only those subgroups share the same encoding pattern.

The reason why this difference is pointed out so explicitly are the immediates. Alpha, for example, allows
16-bit \textbf{signed} displacements for memory instructions whereas most of AArch64's memory instructions
allow only \textbf{unsigned} displacements. The smallest negative offset is -255 for the unscaled load and
store instructions. In turn, AArch64 has more advanced memory instructions including optionally scaled and
pre- or post-indexed offsets. As negative offsets are common in CACAO generated code, negative offsets have
to be loaded into a register using a \texttt{mov} instruction prior to the load or store instruction. See 
chapter \ref{sec:implementation:challenges} why this created problems.

\noindent\begin{minipage}{0.45\textwidth}
\begin{lstlisting}[caption=Alpha code]
ldq     $1, -1024($2)
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{0.45\textwidth}
\begin{lstlisting}[caption=AArch64 code]
mov     x3, #-1024
ldr     x1, [x2, x3] 
\end{lstlisting}
\end{minipage}


\subsection{Load Address instruction}

As the \texttt{lda} instruction is heavily used in generated Alpha code, it is pointed out that AArch64
does not have a corresponding counterpart. Instead, the \texttt{lda} instruction is replaced by \texttt{add}
and \texttt{sub} instructions, depending on the sign of the displacement.

\noindent\begin{minipage}{0.45\textwidth}
\begin{lstlisting}[caption=Alpha code]
lda     $1, -32($2)
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{0.45\textwidth}
\begin{lstlisting}[caption=AArch64 code]
sub     x1, x2, #32
\end{lstlisting}
\end{minipage}


\subsection{Scaling of operands in instructions}

Alpha does not allow scaled operands by itself but rather provides instructions for scaled addition and
subtraction. These instructions come in a 4-bit shift amount and 8-bit variant.
AArch64 allows shift amounts
in the range of 0 to 63 bits (in the 64-bit variant) as well as left and right shifts 
(logical and arithmetical) for multiple instructions. 
The listings show the operation \texttt{r3 = r2 + shift(r1, 3)}.

\noindent\begin{minipage}{0.45\textwidth}
\begin{lstlisting}[caption=Alpha code]
s8addq  $2, $1, $3
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{0.45\textwidth}
\begin{lstlisting}[caption=AArch64 code]
add     x3, x2, x1, LSL 3
\end{lstlisting}
\end{minipage}


\subsection{Comparison and conditional branch instructions}

This is one of the functional groups where the two architectures differ the most. Alpha provides conditional
branch instructions (\texttt{beq}, \texttt{bne}, \dots) which compare a register to the value 0 and 
branch according to the instruction variant used.
Alpha also provides comparison instructions (\texttt{cmpeq}, \texttt{cmplt}, \dots) which allow 
comparison between two registers. These comparison
instructions do not set any status flags, but instead write the result of the comparison (either 0 or 1) into
a third register.

AArch64 uses a single compare instruction (\texttt{cmp}) which sets the condition flags (NZCV).
The conditional branch instruction (\texttt{b.eq}, \texttt{b.le}, \dots) then branches 
based on the condition code used. In addition, AArch64
provides the instructions \texttt{cbz} and \texttt{cbnz} which branch when the given operand register
contains the value 0 or not 0, depending on the instruction used. 

The example shows a comparison between
two registers and a branch if the values in the registers are equal.

\noindent\begin{minipage}{0.45\textwidth}
\begin{lstlisting}[caption=Alpha code]
cmpeq   $0, $1, $2
bne     $2, 0xbabe
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{0.45\textwidth}
\begin{lstlisting}[caption=AArch64 code]
cmp     x0, x1
b.eq    0xbabe
\end{lstlisting}
\end{minipage}
