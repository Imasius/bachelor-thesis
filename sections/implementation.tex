
\section{AArch64 code generator for CACAO}
\label{sec:implementation}

This chapter describes concrete implementation details of the AArch64 code generator and other
AArch64 specific parts of the CACAO VM. It highlights what has been implemented, where problems arose,
which parts are still missing and how future improvements might look like.


\subsection{Calling Convention}

The general Application Binary Interface (ABI) is a guideline for operating systems and compiler developers
how procedures or methods should be called. The ABI for AArch64 is described in the manual \cite{arm:pcs}.
The calling convention used by CACAO for AArch64 uses only a subset from the official ABI, mainly the part
about register usage. Argument passing is done the same way as it is done on Alpha. 


\subsubsection{Register Assignment}

The registers are assigned as described in the ABI. This is possible as we have 32 registers at our
disposal and the ABI designates some of those registers as inter-procedure-call registers (X16 - X18).
See table \ref{tab:implementation:registers} for the used register allocation. 
Note that all registers but X19 - X28 are caller
saved registers whereas registers X19 - X28 have to be saved by the callee.

\begin{table}[h]
\centering
\begin{tabularx}{0.9\textwidth} { ccX }
    \toprule
    \textbf{Register} & \textbf{CACAO Name} & \textbf{Description} \\
    \midrule

    X0             & RESULT            & Result register      \\ 
    X0 - X7        & A0 - A7           & Argument registers   \\ 
    X9             & ITMP1 / XPTR      & Temporary register 1, exception pointer \\ 
    X10            & ITMP2             & Temporary register 2                    \\ 
    X11            & ITMP3             & Temporary register 3                    \\ 
    X12 - X15      &                   & Other temporary registers               \\ 
    X16 (IP0)      & METHODPTR         & Pointer from where PV has been fetched  \\ 
    X17 (IP1)      & PV                & Procedure vector                        \\ 
    X18            & XPC               & Exception program counter               \\
    X19 - X28      & S0 - S9           & Callee saved registers                  \\ 
    X30            & LR                & Link register (return address)          \\ 
    SP / XZR       & SP                & Stack pointer                           \\ 
    \bottomrule
\end{tabularx}
\caption{Register usage in CACAO JIT code}
\label{tab:implementation:registers}
\end{table}

The eight argument registers X0 to X7 are used to pass arguments to methods. Register X0 is also used
as a result register. The register X8, which is the \emph{indirect result location} register, is not used
by CACAO.

The first three of the seven temporary registers are used as ITMP1 - ITMP3 which are the temporary
registers when generating code for specific ICMDs (see \texttt{codegen.cpp}).

The X17 (IP1 or intra-procedure-call scratch register) is used as Procedure Vector (PV) and the platform register
X18 is used as the exception program counter (XPC).

The link register and stack pointer retain their usual behaviour. Special care has to be taken when using
the SP register in instructions as it depends on the instruction, whether the SP or the zero register is used.
An example is the \texttt{mov} instruction which comes in two forms: \texttt{mov} from/to SP (alias of 
\texttt{add}) and \texttt{mov} between registers (alias of \texttt{orr}). To provide compatibility with
the existing CACAO code, a small abstraction has been built which chooses the \texttt{mov} from/to SP
if one of the given arguments is the SP; essentially \enquote{disabling} a \texttt{mov} from/to the zero
register.


\subsubsection{Argument passing}

As mentioned above, argument passing does work nearly the same as on Alpha. AArch64 has 8 integer and 8
floating-point argument registers, whereas Alpha has 6 integer and 6 floating-point argument registers.
The remaining arguments are passed on the stack where, on both architectures, each argument gets its
own 8 byte stackslot.

For a method where integer and floating-point arguments are mixed, the arguments are assigned based on
their position to respective registers, ignoring the type of the argument.

\begin{minipage}{\textwidth}
\begin{lstlisting}[language=Java, basicstyle=\sffamily\small, caption=Example method, 
    label=sec:implementation:method, frame=none,
    xleftmargin=.1\textwidth, xrightmargin=.1\textwidth]
void doSomething(int a, double b, long c, float d);
\end{lstlisting}
\end{minipage}

In the example method (listing \ref{sec:implementation:method}) argument \texttt{a} would be stored in register
\texttt{x0}, argument \texttt{b} in register \texttt{d1}, argument \texttt{c} in register \texttt{x2}
and argument d in register \texttt{d3}.


\subsection{Code generation}

Alpha code generation was implemented using a set of macros. As A64 instructions can not be mapped one to one
on Alpha instructions, re-implementing these macros for AArch64 was not feasible. In addition, macros are 
also limited in terms of readability and type safety. The solution is the introduction of the \texttt{AsmEmitter}
class which currently lives in \texttt{codegen.hpp} and, in essence, represents the A64 instruction set.
\texttt{AsmEmitter} does not only contain A64 instruction generation, but also some abstractions and 
helper methods which will generate more than one instruction or hide the complexity of some instructions.

Examples for helper methods are \texttt{iconst} and \texttt{lconst}, which will load an immediate value into
a register using a \texttt{mov} instruction if the immediate is in range, or otherwise, a load from 
the data segment. Examples for methods which hide complexity are load and store instructions. The \texttt{ldr}
and \texttt{ldur} share nearly the same instruction encoding and are even ambigous for certain offsets, meaning
that a single encoded instruction could be decoded into two different load instructions. The reference manual
clearly states how offsets need to be encoded and how the architecture resolves ambiguity. These rules are
implemented in \texttt{AsmEmitter}.

\texttt{AsmEmitter} was introduced in the middle of the porting process which results in an inconsistent
implementation. The real code generation happens in \texttt{emit-asm.hpp} which contains inline \emph{functions}
for every encoding group. These inline functions are then wrapped by macros which represent a single
instruction variant. Currently the \texttt{AsmEmitter} only references these macros in \texttt{emit-asm.hpp}
although the implementation might be subject to change, depending on future performance and readability issues
as well as code size of CACAO itself. Listing \ref{sec:implementation:addsub} shows the encoding 
function for the \emph{Add/subtract immediate} functional group.

\begin{lstlisting}[language=C, basicstyle=\sffamily\small, caption=Add/subtract (immediate), 
        label=sec:implementation:addsub, frame=none, tabsize=2]

inline void emit_addsub_imm(codegendata *cd, u1 sf, u1 op, u1 S, 
                            u1 shift, u2 imm12, u1 Rn, u1 Rd)
{
	assert(imm12 >= 0 && imm12 <= 0xfff);
	*((u4 *) cd->mcodeptr) = LSL(sf, 31) | LSL(op, 30) | LSL(S, 29) 
	    | LSL(shift, 22) | LSL(imm12, 10) | LSL(Rn, 5) 
	    | Rd | 0x11000000;
	cd->mcodeptr += 4;
}

\end{lstlisting}

The argument ordering and naming is as close as possible to the reference manual. Most instructions
have the \texttt{sf} flag which determines the variant (32 or 64-bit) of the instruction. Usually
the width of immediates is encoded in the variable names to help readability. For further details 
the reference manual is the place to look \cite{arm:ref}.

\begin{lstlisting}[language=C, basicstyle=\sffamily\small, caption=Add/subtract helper, 
        label=sec:implementation:addsubhelper, frame=none, tabsize=2]

inline void emit_addsub_imm(codegendata *cd, u1 sf, u1 op, 
                            u1 Xd, u1 Xn, u4 imm)
{
	assert(imm >= 0 && imm <= 0xffffff);
	u2 lo = imm & 0xfff;
	u2 hi = (imm & 0xfff000) >> 12;
	if (hi == 0)
		emit_addsub_imm(cd, sf, op, 0, 0, lo, Xn, Xd);
	else {
		emit_addsub_imm(cd, sf, op, 0, 1, hi, Xn, Xd);
		emit_addsub_imm(cd, sf, op, 0, 0, lo, Xd, Xd);
	}
}

\end{lstlisting}

Listing \ref{sec:implementation:addsubhelper} shows a small helper function, which allows 
addition and subtraction for larger immediates by emitting an additional instruction 
for the higher 12 bits of the immediate.

Listing \ref{sec:implementation:addsubmacros} shows a subset of the macros which use the 
two functions presented in listing \ref{sec:implementation:addsub} and \ref{sec:implementation:addsubhelper}.
In contrast to the encoding functions, the macros order and name the arguments as they would be
in assembler code. The listing shows that the \texttt{cmp} with immediate instruction is just an alias
of the \texttt{subs} instruction using the zero register as destination registers. \texttt{subs} operates
the same way as a normal \texttt{sub}, but additionally sets the status flags.

\begin{lstlisting}[language=C, basicstyle=\sffamily\small, caption=Add/subtract macros, 
        label=sec:implementation:addsubmacros, frame=none]

#define emit_add_imm(cd, Xd, Xn, imm)	
        emit_addsub_imm(cd, 1, 0, Xd, Xn, imm)
#define emit_add_imm32(cd, Wd, Wn, imm)	
        emit_addsub_imm(cd, 0, 0, Wd, Wn, imm)

#define emit_mov_sp(cd, Xd, Xn)			
        emit_add_imm(cd, Xd, Xn, 0)

#define emit_subs_imm(cd, Xd, Xn, imm)		
        emit_addsub_imm(cd, 1, 1, 1, 0, imm, Xn, Xd)

#define emit_cmp_imm(cd, Xn, imm)		
        emit_subs_imm(cd, 31, Xn, imm)

\end{lstlisting}

The \texttt{AsmEmitter} class now simply delegates calls to the respective macros (listing 
\ref{sec:implementation:asmemitter}). Keep in mind that the \texttt{codegendata} pointer is a member
of the \texttt{AsmEmitter} class. The naming convention is oriented towards CACAO where a prefix
describes the datatype for which this instruction is intended.

\begin{lstlisting}[language=C, basicstyle=\sffamily\small, caption=\texttt{AsmEmitter} excerpt, 
        label=sec:implementation:asmemitter, frame=none, tabsize=2]

class AsmEmitter {
	
    public:
	    explicit AsmEmitter(codegendata *cd) : cd(cd) {}
        
        ...

        void iadd_imm(u1 wd, u1 wn, u4 imm) { 
            emit_add_imm32(cd, wd, wn, imm); 
        }

        void ladd_imm(u1 xd, u1 xn, u4 imm) { 
            emit_add_imm(cd, xd, xn, imm); 
        }

        ...
}


\end{lstlisting}

A future refactoring might
get rid of the macros by simply implementing the inline functions as private methods and call those methods
directly, building helper methods as needed.


\subsubsection{Implementing ICMDs}

The heart of code generation is a big \texttt{switch} in \texttt{codegen.cpp}. Inside, each ICMD has to
be implemented by emitting appropriate machine code. Most of the ICMDs are straightforward to implement.
Listing \ref{sec:implementation:iadd} shows the implementation of the \texttt{ICMD\_IADD}.

\begin{lstlisting}[language=C, basicstyle=\sffamily\small, caption=\texttt{ICMD\_IADD} implementation, 
        label=sec:implementation:iadd, frame=none, tabsize=2]

case ICMD_IADD:       

    s1 = emit_load_s1(jd, iptr, REG_ITMP1);
    s2 = emit_load_s2(jd, iptr, REG_ITMP2);
    d = codegen_reg_of_dst(jd, iptr, REG_ITMP2);

    asme.iadd(d, s1, s2);

    emit_store_dst(jd, iptr, d);
    break;


\end{lstlisting}

Other ICMDs are more complex and cumbersome to implement. The most complex are the \texttt{ICMD\_INSTANCEOF} and
\texttt{ICMD\_CHECKCAST} which have to resolve and compare types at runtime.


\subsection{Trap handling}

Traps are used for exception handling as well as switching context from the running Java program back to
CACAO if needed. One of the most important traps is the compiler trap. The compiler trap will occur when
a Java method is called which is not yet compiled. Listing \ref{sec:implementation:trap} shows the trap
numbers for AArch64 as well as an overview of which traps exist.

\begin{lstlisting}[language=C, basicstyle=\sffamily\small, caption=Traps defined in \texttt{md-trap.hpp}, 
        label=sec:implementation:trap, frame=none, tabsize=2]

enum {
	TRAP_NullPointerException           = 0,
	TRAP_ArithmeticException            = 2,
	TRAP_ArrayIndexOutOfBoundsException = 3,
	TRAP_ArrayStoreException            = 4,
	TRAP_ClassCastException             = 5,
	TRAP_CHECK_EXCEPTION                = 6,
	TRAP_PATCHER                        = 7,
	TRAP_COMPILER                       = 8,
	TRAP_COUNTDOWN                      = 9
};

\end{lstlisting}

How traps are implemented differs from architecture to architecture. Alpha uses load instructions with illegal
offsets encoding the trap number and a register in the instruction. This generates an \texttt{SIGILL} at
runtime which can be caught and acted upon with a signal handler.

\begin{figure*}[ht]
\centering
\begin{bytefield}[endianness=big]{32}
    \bitheader{0,7,15,23,31} \\
    \bitbox{1}{1} & \bitbox{1}{1} & \bitbox{1}{1} & \bitbox{1}{0} &
    \bitbox{1}{0} & \bitbox{1}{1} & \bitbox{1}{1} & \bitbox{1}{1} & 
    \bitbox{8}{Unused} & \bitbox{8}{Trap} & \bitbox{8}{Register} \\
\end{bytefield}
\caption{Illegal trap instruction encoding}
\end{figure*}

AArch64 uses the unallocated instruction space to emit an illegal instruction. The unallocated instruction space
is denoted by bits 27 and 28 being 0 in an encoded instruction. The register is encoded in the lowest byte and
the trap number in the second lowest byte of this illegal instruction.


\subsection{Challenges and problems}
\label{sec:implementation:challenges}

As in every new and unfamiliar software project, problems and errors occur due to incomplete or just plain
wrong understanding of how internals work. Other challenges were caused by differences between Alpha and AArch64.
Some of them are described below.


\subsubsection{Small immediates and their impact}

One example where small immediates did cause problems is the recalculation of the procedure vector or PV. 
The PV has to be recalculated after every Java method call and is basically a simple subtraction. Alpha uses
its \texttt{lda} instruction for this purpose which allows a 16 bit signed offset while AArch64's \texttt{sub}
instruction only allows a 12 bit unsigend offset. This is not a problem at code generation 
time as simply two \texttt{sub}
instructions can be emitted, but rather at runtime, when the offset has to be extracted out of these
instructions (as is the job of the function \texttt{md\_codegen\_get\_pv\_from\_pc}). Extracting the offset out
of one and an optional second instruction introduces more complexity.


\subsubsection{Load/store ambiguity and the consequences for patching}

Not every offset, index or address is known at code generation time. This results in different patcher functions
which change existing machine instructions or memory slots in the data segment at runtime, 
as soon as the necessary information is available.
The patcher functions are highly coupled to the instructions used. On Alpha, patching in a new offset for a
load instruction is simply done by changing the lower 2 bytes of the load instruction to this new offset. 

AArch64 does have offset ambiguity, meaning that the whole instruction has to be reencoded based on the new
offset. Not doing so would risk a wrong interpretation of the offset, in ambiguous cases, by the processor.


\subsection{Remaining work}

As mentioned earlier, the time frame of a bachelor thesis does not allow for a full and compliant port.
This section gives some pointers to topics which need improvement or are not implemented at all.
At the time of this writing, simple programs are executed as expected. 

CACAO does also include an extensive test suite which exercises various parts of the VM. At this time only the
tests in \texttt{regression/base} are executed correctly.


\subsubsection{Inefficient instructions}

Some ICMDs or instruction combinations could be implemented more efficiently. These include
the \texttt{iconst} and \texttt{lconst} methods, which only use one \texttt{mov} instruction and a load from
the data segment if the immediate is larger than 2 bytes. Larger immediates could be immediatly loaded into
a register using a combination of \texttt{mov} and \texttt{movk} instructions, effectively removing a load.

Logical ICMDs involving a constant, for example \texttt{ICMD\_IORCONST}, do \emph{not} use the immediate
variants of the respective instructions but instead use a \texttt{iconst} or \texttt{lconst} to load
the immediate into a register and use the register variant of the instruction. This was implemented because the
encoding of the immediate variants is more complex.


\subsubsection{Advanced instructions}

AArch64 provides some more advanced instructions which are not utilized at all. These include pre- and
post-increment load and store instructions or load and store pairs. The pair instructions allow loading or
storing two registers in a single instruction, provided that they are stored successively in memory 
(Listing \ref{sec:implementation:pair} shows an example). 

\begin{minipage}{\textwidth}
\begin{lstlisting}[caption=Load pair, 
    label=sec:implementation:pair, frame=none]

ldp x0, x1, [sp, #32]

\end{lstlisting}
\end{minipage}

Also advanced floating-point and SIMD instructions 
are not used at all besides basic floating-point arithmetic and conversion. To utilize these advanced
instructions in most cases, a more sophisticated instruction selection has to be implemented, which is
currently done in the next iteration of the CACAO VM, the compiler2 \cite{eisl:2013}.


\subsubsection{Execution on real hardware}

All the described work has been done using a simulator. The simulator documentation \cite{arm:fm} describes
the most important limitations of the model: 

\begin{itemize}
    \item Caches are modelled as stateless
    \item Write buffers are not modelled
    \item Interrupts are not taken at every instruction boundary
    \item No support for Thumb2EE
    \item No Support for the ARMv8 cryptography extensions
\end{itemize}

To ensure that the AArch64 port is really implemented correctly, execution on real hardware is necessary.
Keep in mind that this might entail more development effort.



