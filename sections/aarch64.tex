
\section{The AArch64 architecture}
\label{sec:aarch64}

The AArch64 architecture was announced by ARM in October 2011. Technically, the overall 
architecture is called
\textbf{ARMv8-A} whereas the 64-bit execution state of the architecture is called \textbf{AArch64} 
and the corresponding instruction set \textbf{A64}.
This was done as ARMv8 preserves backwards compatibility where the 32-bit execution state is now known
as \textbf{AArch32}.

The ARM Architecture Reference Manual \cite{arm:ref} describes ARMv8 as a RISC architecture with 
the following features:
\begin{itemize}
    \item A large uniform register file.
    \item A \emph{load/store} architecture, where data-processing operations only operate on register contents,
        not directly on memory contents.
    \item Simple addressing modes, with all load/store addresses determined from register contents and
        instruction fields only.
\end{itemize}


\subsection{Data Types}

The ARMv8 architecture supports the integer data types \textbf{Byte} (8 bits), \textbf{Halfword} (16 bits),
\textbf{Word} (32 bits), \textbf{Doubleword} (64 bits) and \textbf{Quadword} (128 bits). It also supports
Half-, Single- and Double-precision floating-point as well as vectors, where a register holds multiple
elements.


\subsection{Registers}

The ARMv8 architecture provides 32 64-bit integer registers denoted by X0 to X30. Registers with
special semantics are register 31, which acts as both stack pointer (SP) and zero register (XZR)
depending on the used instruction, and register X30, which acts as link or return address register (LR).
The bottom 32-bit of each register can be accessed using W0 to W30. 

ARMv8 also provides 32 128-bit SIMD vector and floating-point registers, V0 to V31. Each register
can be accessed as:
\begin{itemize}
    \item A 128-bit register named Q0 to Q31.
    \item A 64-bit register named D0 to D31.
    \item A 32-bit register named S0 to S31.
    \item A 16-bit register named H0 to H31.
    \item An 8-bit register named B0 to B031.
\end{itemize}

\begin{table}[h]
\centering
\begin{tabularx}{0.75\textwidth} { cX }
    \toprule
    \textbf{Register} & \textbf{Standard register usage convention} \\ 
    \midrule
    X0 - X7        & Parameter/result registers                   \\ 
    X8             & Indirect result location register            \\ 
    X9 - X15       & Scratch registers (caller saved)             \\ 
    X16            & \textbf{IP0}, first intra-procedure-call scratch register \\ 
    X17            & \textbf{IP1}, second intra-procedure call scratch register \\ 
    X18            & Platform register if needed, temporary otherwise \\ 
    X19 - X28      & Callee saved registers                       \\ 
    X29            & \textbf{FP}, the frame pointer               \\ 
    X30            & \textbf{LR}, the link register               \\ 
    SP             & The stack pointer                            \\ 
    \bottomrule
\end{tabularx}
\caption{AArch64 Integer registers and their purpose}
\label{sec:aarch64:registers}
\end{table}


\subsection{The A64 Instruction Set}

The most important thing to note is that all A64 instructions have a fixed width of 32 bits. Also,
instructions must be word aligned. The instruction encoding is more complex than the Alpha counter part
and grouped into functional groups:
\begin{itemize}
    \item Branch, exception generating and system instructions
    \item Data processing instructions operating on the general-purpose registers
    \item Load and store instructions for both general-purpose and SIMD and floating-point registers
    \item SIMD and scalar floating-point data processing instructions operating on the SIMD and floating-point
        registers
\end{itemize}

Most of the instructions also come in two variants. The 32-bit variant operates on the lower 32 bits of
the specified registers whereas the 64-bit variant operates on the whole register.


\subsection{AAarch32 execution state and floating-point}

Every processor implementing the ARMv8-A architecture is \textbf{required} to not only provide the AArch64
execution state, but also the AArch32 execution state. The instruction set of the AArch32 execution state
is now named A32, but it is widely known simply as ARM, as it is the instruction set implemented in previous
ARM architectures (ARMv7 and so on). 

Transitioning between the AArch64 and AArch32 execution states is known
as \emph{interprocessing}. Interprocessing can only happen when the exception level changes, meaning that
an application may run in AArch64 while the operating system is running in AArch32. Transitioning between
execution states inside a single application is not possible, so an application has to run either in AArch64
or AArch32.

ARMv8 can support different levels of support for floating-point and Advanced SIMD instructions, namely either
full floating-point and SIMD support, with or without exception trapping, or no floating-point or SIMD 
support. The last option is only licensed for very specialised hardware as full floating-point and SIMD
support is required by the ARM Procedure Call Standard \cite{arm:pcs}. This means essentially that if an operating
system is running on the hardware, there is also full floating-point and SIMD support.
