
\section{Evaluation}
\label{sec:evaluation}

Comparing CACAO VM on different architectures is not easy and all measured results have to be taken with a
grain of salt. There are a lot of factors which influence the runtime as well as the compile time
behaviour and performance of CACAO. Just to name a few:

\begin{itemize}
    \item Compiler and compiler flags used for CACAO itself
    \item Configure options (especially optimizations)
    \item Hardware used
\end{itemize}

As a simulator was used for development, statements about the performance of the AArch64 implementation will
be deferred until hardware is available for testing. Nontheless, there are some compile time statistics 
which are interesting. One of them is the size of the generated machine code. Another is the number of local
variables held in registers versus the number of local variables held in memory. 
As AArch64 provides more general purpose
registers than previous ARM architectures or the AMD64 architecture, the number of variables held in registers
is estimated higher for AArch64.

To get meaningful results, a high number of methods needs to be compiled. This can be achieved using the
\texttt{-XX:+CompileAll} option of CACAO which will compile \emph{every} method found on the classpath.
Unfortunately, the \texttt{-XX:+CompileAll} option does not work at the time of this writing, neither for
AArch64 nor for AMD64. 
As a result, only two test cases are used. The first is a simple \enquote{Hello World} application. 
The second is a run
of all test cases in the \texttt{regression/base} directory of the CACAO test suite.


\subsection{Comparing compile time statistics for ARMv8, ARM and AMD64}

Versions used for CACAO are the latest development versions of the respective architecture. The \texttt{javac}
version used for compiling the examples is 1.6.0\_32. The JUnit version used for running the second test case is
4.4.11. Classpath was used in version 0.98 although it was compiled with different settings for the different
architectures. The test classes were compiled using the following \texttt{javac} line:

\begin{lstlisting}[frame=none]
javac -source 1.5 -target 1.5 \
    -cp /usr/share/java/junit4.jar:. *.java
\end{lstlisting}

For these compile time statistics the platform of the host, on which CACAO is run, is not that important.
An ASUS notebook with an Intel Core i7-3610QM was used for running the AMD64 test cases and the simulator
which ran the AArch64 test cases. The ARM test cases were executed on a PandaBoard.

As can be seen in table \ref{tab:evaluation:size}, the generated code size for AArch64 lies between ARM
and AMD64 in both cases
whereas the data size is larger. An explanation for the bigger data size might be the fact, that CISC 
architectures provide instructions which operate directly on memory. Also, AMD64 allows 64-bit
immediates in instructions where AArch64 might use a load from the data segment.

\begin{table}[ht]
\centering
\begin{tabularx}{\textwidth} { llrrr }
    \toprule
    Program         & Architecture      & Code len (byte) & Data len (byte) & Sum \\
    \midrule
    \texttt{Hello}  & AArch64           & 259952           & 36260       & 296212 \\
                    & ARM               & 236460           & 27800       & 264260 \\
                    & AMD64             & 276993           & 30140       & 307133 \\

    \midrule
    \texttt{regression/base} &  AArch64 & 670432           & 123644      & 794076 \\
                    & ARM               & 615692           & 84800       & 700492 \\
                    & AMD64             & 709524           & 105504      & 815028 \\

    \bottomrule
\end{tabularx}
\caption{Generated code and data size}
\label{tab:evaluation:size}
\end{table}

Table \ref{tab:evaluation:spills} shows the number of local variables held in registers versus 
the number of local variables held in memory. As expected, the number of register held variables is
significantly higher for AArch64 as it is for AMD64 and ARM which might lead to a performance improvement
in certain situations.

\begin{table}[ht]
\centering
\begin{tabularx}{\textwidth} { llrrr }
    \toprule
    Program         & Architecture      & Locals registers & Locals memory \\
    \midrule
    \texttt{Hello}  & AArch64           & 1126             & 35            \\
                    & ARM               & 978              & 201           \\
                    & AMD64             & 958              & 203           \\

    \midrule
    \texttt{regression/base} &  AArch64 & 4568             & 296           \\
                    & ARM               & 3541             & 1151          \\
                    & AMD64             & 3717             & 1062          \\

    \bottomrule
\end{tabularx}
\caption{Register held vs. memory held variables}
\label{tab:evaluation:spills}
\end{table}

It is important to note that the provided test cases are relatively small due to the fact that the
AArch64 simply needs more work until larger programs can be run correctly. More representative data would be
generated by executing larger programs such as \emph{eclipse} or an application server like \emph{Wildfly}. 
